#!/bin/python3
"""
Welcome. In this kata, you are asked to square every digit of a number.

For example, if we run 9119 through the function, 811181 will come out,
because 92 is 81 and 12 is 1.

Note: The function accepts an integer and returns an integer
"""


def square_digits(num):
    """
    Squares every digit of a number.

    :param num:     input number, type: int
    :return:        resulted number, type: int
    """

    new_int = ""
    for digit in str(num):
        new_int += str(int(digit)**2)

    return int(new_int)


print(square_digits(9119))
