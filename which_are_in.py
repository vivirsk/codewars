#!/bin/python3
"""
Given two arrays of strings a1 and a2 return a sorted array r in
lexicographical order of the strings of a1 which are substrings of strings of
a2.

#Example 1: a1 = ["arp", "live", "strong"]

a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

returns ["arp", "live", "strong"]

#Example 2: a1 = ["tarp", "mice", "bull"]

a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

returns []
"""


def in_array(array1, array2):

    def func(word):
        for j in array2:
            if word in j:
                return True

    return sorted(list(set([i for i in filter(func, array1)])))


a1 = ["live", "arp", "strong"]
a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
print(in_array(a1, a2))

a1 = ["tarp", "mice", "bull"]
print(in_array(a1, a2))

a1 = ["duplicate", "duplicate"]
a2 = ["duplicates"]
print(in_array(a1, a2))
