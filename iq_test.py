#!/bin/python3
"""
Bob is preparing to pass IQ test. The most frequent task in this test is to
find out which one of the given numbers differs from the others. Bob observed
that one number usually differs from the others in evenness. Help Bob — to
check his answers, he needs a program that among the given numbers finds one
that is different in evenness, and return a position of this number.

! Keep in mind that your task is to help Bob solve a real IQ test, which means
indexes of the elements start from 1 (not 0)
"""


def iq_test(numbers):
    numbers = list(map(int, numbers.split(" ")))
    odd = []
    even = []

    for num in numbers:

        if num % 2 == 0:
            even.append(num)
        else:
            odd.append(num)

        if (len(odd) > 1 and len(even) == 1) or \
                (len(even) > 1 and len(odd) == 1):
            result = even[0] if len(even) == 1 else odd[0]
            return numbers.index(result) + 1


print(iq_test("2 4 7 8 10"))
print(iq_test("1 2 2"))
print(iq_test("1 2 1 1"))
