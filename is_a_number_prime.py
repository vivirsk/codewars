#!/bin/python3
"""
Is Prime
Define a function isPrime/is_prime() that takes one integer argument and
returns true/True or false/False depending on if the integer is a prime.

Per Wikipedia, a prime number (or a prime) is a natural number greater than 1
that has no positive divisors other than 1 and itself.

Example
bool isPrime(5) = return true

Assumptions
- You can assume you will be given an integer input.
- You can not assume that the integer will be only positive. You may be given
  negative numbers as well (or 0).
"""


def is_prime(num):
    div = [2, 3, 5, 7]
    if num in div:
        return True
    return False if num <= 1 else not any(num % i == 0 for i in div)


print(is_prime(0))
print(is_prime(1))
print(is_prime(2))
