#!/bin/python3
"""
In this kata, you must create a digital root function.

A digital root is the recursive sum of all the digits in a number. Given n,
take the sum of the digits of n. If that value has two digits, continue
reducing in this way until a single-digit number is produced. This is only
applicable to the natural numbers.
"""


def digital_root(n):

    if len(str(n)) < 2:
        return n

    result = sum([int(digit) for digit in str(n)])

    return digital_root(result)


print(digital_root(16))         # 7
print(digital_root(942))        # 6
print(digital_root(132189))     # 6
print(digital_root(493193))     # 2

